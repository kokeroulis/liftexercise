package com.antonis.liftexample;


 final class LiftAlgorithm {

    int calculateLiftTicks(int[] A, int[] B, int F, int P, int W) {
        int currentSize = 0;
        int peopleServed = 0;
        int tickCount = 0;

        int currentFloor = 0;
        int peopleInRound = 0;
        do {
            System.out.println("tick increased " + tickCount);
            final int index = indexOfPersonInFloor(B, currentFloor);
            if (index >= 0) {
                System.out.println("handle people at floor " + currentFloor);
                currentSize += A[index];
                peopleServed++;
                tickCount++;
                peopleInRound++;
            }

            currentFloor++;
            tickCount++;
            if (peopleInRound == P || currentFloor == F || currentSize >= W) {
                tickCount += F - currentFloor; // go to ground
                currentFloor = 0;
                peopleInRound = 0;
            }
        } while (peopleServed != B.length);

        return tickCount;
    }

    private int indexOfPersonInFloor(int[] B, int floor) {
        for (int i = 0; i < B.length; i++) {
            if (B[i] == floor) {
                return i;
            }
        }

        return -1;
    }
}
