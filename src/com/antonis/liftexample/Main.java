package com.antonis.liftexample;


public class Main {

    public static void main(String[] args) {
        final int[] A = new int[] {60, 80, 40};
        final int[] B = new int[] {2, 3, 2};
        final int P = 2;
        final int W = 200;
        final int F = 5;

        LiftAlgorithm algorithm = new LiftAlgorithm();
        final int tickCount = algorithm.calculateLiftTicks(A, B, F, P, W);
        System.out.println(tickCount);
    }
}
